import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class ProviderView<T extends ChangeNotifier> extends StatefulWidget {
  final ValueWidgetBuilder builder;
  final ChangeNotifier model;
  final Function onReady;

  const ProviderView(
      {Key key, @required this.builder, @required this.model, this.onReady})
      : super(key: key);

  @override
  _ProviderViewState createState() => _ProviderViewState();
}

class _ProviderViewState<T extends ChangeNotifier> extends State<ProviderView> {
  T model;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    model = widget.model;
    widget.onReady?.call(model);
  }

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider<T>(
        create: (BuildContext context) => widget.model,
        child: Consumer<T>(builder: widget.builder));
  }
}
