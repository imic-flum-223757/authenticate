import 'base_view_model.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class BaseListViewModel extends BaseViewModel {
  final refreshController = RefreshController(initialRefresh: false);
  int _page = 1;

  get page => _page;

  setPage(page) {
    _page = page;
    notifyListeners();
  }
}
