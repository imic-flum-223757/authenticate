import 'package:authenticate/src/services/auth_service.dart';
import 'package:authenticate/src/view_models/auth_view_model.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'src/router.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(create: (_) => AuthService(),
      child: Consumer<AuthService>(
        builder: (_, model, __) {
          return MultiProvider(
              providers: [
                ChangeNotifierProvider<AuthViewModel>(
                  create: (_) => AuthViewModel(model),
                )
              ],
              child: MaterialApp(
                title: 'Flutter Demo',
                theme: ThemeData(
                  primarySwatch: Colors.blue,
                  visualDensity: VisualDensity.adaptivePlatformDensity,
                ),
                onGenerateRoute: RouteGenerator.generateRoute,
                initialRoute: 'splash',
              ));
        },)
      ,
    );
  }
}
