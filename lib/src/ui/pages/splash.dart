// import 'package:authenticate/src/view_models/auth_view_model.dart';
// import 'package:flutter/material.dart';
// import 'package:provider/provider.dart';
//
// class SplashPage extends StatefulWidget {
//   @override
//   _SplashPageState createState() => _SplashPageState();
// }
//
// class _SplashPageState extends State<SplashPage> {
//   AuthViewModel model;
//
//   @override
//   void initState() {
//     // TODO: implement initState
//     model = Provider.of<AuthViewModel>(context);
//     model.tryLogin(context);
//     super.initState();
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(body: loading());
//   }
//
//   Widget loading() {
//     return Center(
//       child: Text('Loading ...'),
//     );
//   }
// }
import 'package:authenticate/core/base/provider_view.dart';
import 'package:authenticate/src/services/auth_service.dart';
import 'package:authenticate/src/view_models/auth_view_model.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class SplashPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final auth = Provider.of<AuthService>(context);

    return ProviderView<AuthViewModel>(
      model: AuthViewModel(auth),
      onReady: (model) => model.tryLogin(context),
      builder: (_, model, __) {
        return Scaffold(
          body: Center(
            child: Text('Loading ...'),
          ),
        );
      },
    );
  }
}
