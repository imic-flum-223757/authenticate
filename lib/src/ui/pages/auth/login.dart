import 'package:authenticate/src/view_models/auth_view_model.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class LoginPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Consumer<AuthViewModel>(
        builder: (_, model, __) {
          return SingleChildScrollView(
            child: SafeArea(
              child: Container(
                color: Colors.red,
                child: Column(
                  children: [
                    TextField(
                      controller: model.usernameCtr,
                    ),
                    TextField(
                      controller: model.passwordCtr,
                    ),
                    RaisedButton(
                      onPressed: () => model.login(context),
                      child: Text('Gửi'),
                    )
                  ],
                ),
              ),
            ),
          );
        },
      ),
    );
  }
}
