import 'dart:ui';

import 'package:authenticate/src/services/auth_service.dart';
import 'package:authenticate/src/view_models/auth_view_model.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:image_picker/image_picker.dart';

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final auth = Provider.of<AuthService>(context);
    return Consumer<AuthViewModel>(
      builder: (_, model, __) {
        return Scaffold(
            body: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Text(
                'This home page ${auth.currentUser != null ? auth.currentUser.name : ''} '),
            RaisedButton(
              onPressed: () => model.logout(context),
              child: Text("Logout"),
            ),
            GestureDetector(
              child: Container(
                height: 100,
                width: 70,
                decoration: BoxDecoration(
                    color: Colors.grey,
                    image: model.image != null
                        ? DecorationImage(image: FileImage(model.image))
                        : null),
              ),
              onTap: () async {
                await model.openImage();
              },
            ),
            RaisedButton(
              onPressed: () => model.updateAvatar(),
              child: Text("Upload image"),
            ),
          ],
        ));
      },
    );
  }
}
