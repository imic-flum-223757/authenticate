import 'package:shared_preferences/shared_preferences.dart';

class AuthRef {
  static Future<SharedPreferences> get prefs => SharedPreferences.getInstance();

  Future<void> setToken(String val) async {
    final p = await prefs;
    p.setString('token', val);
  }

  Future<String> getToken() async {
    final p = await prefs;
    return p.getString('token');
  }

  void removeToken() async {
    final p = await prefs;
    p.remove('token');
  }
}
