import 'dart:convert';
import 'dart:io';

import 'package:authenticate/src/data/refs/auth_ref.dart';
import 'package:http/http.dart' as http;

class BaseRepository {
  final _authR = AuthRef();
  final _endpoint = "flum.monitork.com";
  var client = new http.Client();

  Future<http.Response> post(String resource, Object data) async {
    var token = await _authR.getToken();
    var uri = Uri.https(_endpoint, '/api' + resource);
    print(uri);
    var headers = {
      'Content-Type': 'application/json',
      'Authorization': 'Bearer $token'
    };
    return await client.post(uri, headers: headers, body: json.encode(data));
  }

  Future<http.Response> get(String resource, Object data) async {
    var uri = Uri.https(_endpoint, '/api$resource', data);
    print(uri);
    var token = await _authR.getToken();
    var headers = {
      'Content-Type': 'application/json',
      'Authorization': 'Bearer $token'
    };
    print(headers);
    return await client.get(uri, headers: headers);
  }

  Future<http.StreamedResponse> postImage(String resource, File img) async {
    var uri = Uri.https(_endpoint, '/api' + resource);
    var request = http.MultipartRequest('POST', uri);
    var token = await _authR.getToken();
    request.headers.addAll({'Authorization': 'Bearer $token'});
    var pic = await http.MultipartFile.fromPath("image", img.path);
    request.files.add(pic);
    return await request.send();
  }
}
