import 'dart:io';

import 'package:http/http.dart';

import 'base_repository.dart';

class AuthRepository {
  final baseR = BaseRepository();

  Future<Response> userLogin(String username, String pass) async {
    return await baseR.post('/login', {'email': username, 'password': pass});
  }

  Future<Response> userProfile() async {
    var res = await baseR.get('/profile', {'ref': '1'});
    print('....$res');
    return res;
  }

  Future<StreamedResponse> userProfileImage(File image) async {
    return await baseR.postImage('/users/avatar', image);
  }
}
