import 'dart:convert';
import 'dart:io';
import 'package:authenticate/src/data/refs/auth_ref.dart';
import 'package:authenticate/src/models/user.dart';
import 'package:authenticate/src/services/auth_service.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:authenticate/core/base/base_view_model.dart';
import 'package:authenticate/src/data/repository/auth_repository.dart';
import 'package:flutter/cupertino.dart';
import 'package:image_picker/image_picker.dart';
import 'package:validators/validators.dart' as validate;

class AuthViewModel extends BaseViewModel {
  final AuthService auth;
  AuthRef _ref = AuthRef();
  File _image;

  File get image => _image;

  TextEditingController usernameCtr = TextEditingController();
  TextEditingController passwordCtr = TextEditingController();
  final _authR = AuthRepository();

  final picker = ImagePicker();

  AuthViewModel(this.auth);

  formValidate(String text) {
    return validate.isNumeric(text) && validate.isLength(text, 10, 10);
  }

  openImage() async {
    var pickedImage;
    try {
      pickedImage = await picker.getImage(
          source: ImageSource.gallery,
          maxHeight: 800,
          maxWidth: 800,
          imageQuality: 30);
    } catch (e) {
      print(e);
    }
    if (pickedImage != null) {
      _image = File(pickedImage.path);
    }
    notifyListeners();
  }

  Future<void> tryLogin(BuildContext context) async {
    print('Bat dau');
    await Future.delayed(Duration(seconds: 3));
    var check = await profile();
    print("Check $check");
    if (check) {
      print('Ket thuc');
      Navigator.of(context).pushNamedAndRemoveUntil('home', (route) => false);
    } else {
      Navigator.of(context).pushNamedAndRemoveUntil('login', (route) => false);
    }
  }

  Future<bool> profile() async {
    try {
      var res = await _authR.userProfile();
      var data = jsonDecode(res.body);
      var user = User.fromJson(data['data']['user']);
      auth.setUser(user);
      return true;
    } catch (error) {
      return false;
    }
  }

  Future<void> login(BuildContext context) async {
    if (this.loading) {
      return;
    }
    if (usernameCtr.text == null && passwordCtr.text == null) {
      Fluttertoast.showToast(
          msg: "Error phone adadasd hànoi âcho cấchdad \n chao ",
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          timeInSecForIosWeb: 1,
          backgroundColor: Colors.red,
          textColor: Colors.white,
          fontSize: 16.0);
      return;
    }

    setLoading(true);
    await _authR
        .userLogin(usernameCtr.text, passwordCtr.text)
        .then((res) async {
      var data = jsonDecode(res.body);
      if (data['token'] != null) {
        await _ref.setToken(data['token']);
      }
    }).catchError((onError) {
      print('$onError');
    });
    if (await profile()) {
      Navigator.of(context).pushReplacementNamed('home');
    }
    setLoading(false);
  }

  logout(BuildContext context) {
    _ref.removeToken();
    auth.setUser(null);
    Navigator.of(context).pushReplacementNamed('login');
  }

  updateAvatar() async {
    try {
      var response = await _authR.userProfileImage(_image);
      if (response.statusCode == 200) {
        response.stream.transform(utf8.decoder).listen((value) {
          print('Image Uploded $value');
        });
      }
    } catch (e) {
      print(e);
    }
  }
}
