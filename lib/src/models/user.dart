import 'package:flutter/material.dart';

class ImageFile {
  final String fileName;
  final int id;

  ImageFile({this.fileName, this.id});

  factory ImageFile.fromJson(Map<String, dynamic> json) {
    return ImageFile(id: json['id'], fileName: json['file_name']);
  }
}

class User {
  final int id;
  final String name;
  final String email;
  final String createdAt;
  final String updatedAt;
  final ImageFile avatar;

  User(
      {this.id,
      this.name,
      this.email,
      this.createdAt,
      this.updatedAt,
      this.avatar});

  factory User.fromJson(Map<String, dynamic> json) {
    print('ID ${json['id']}');
    return User(
        id: json['id'] as int,
        name: json['name'],
        createdAt: json['created_at'] ?? null,
        updatedAt: json['updated_at'] ?? null,
        email: json['email'] ?? null,
        avatar:
            json['avatar'] != null ? ImageFile.fromJson(json['avatar']) : null);
  }
}
