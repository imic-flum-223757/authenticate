import 'package:authenticate/src/ui/pages/auth/login.dart';
import 'package:authenticate/src/ui/pages/home/home.dart';
import 'package:authenticate/src/ui/pages/splash.dart';
import 'package:flutter/material.dart';

const String initialRoute = 'splash';

class RouteGenerator {
  static Route<dynamic> generateRoute(RouteSettings settings) {
    final args = settings.arguments;
    switch (settings.name) {
      case 'home':
        return _getPageRoute(
          routeName: settings.name,
          viewToShow: HomePage(),
        );
      case "splash":
        return _getPageRoute(
          routeName: settings.name,
          viewToShow: SplashPage(),
        );
      case "login":
        return _getPageRoute(
          routeName: settings.name,
          viewToShow: LoginPage(),
        );
      // case "otp":
      //   return _getPageRoute(
      //     routeName: settings.name,
      //     viewToShow: OtpPage(),
      //   );
      default:
        return MaterialPageRoute(
            builder: (_) => Scaffold(
                    body: Center(
                  child: Text('No Router'),
                )));
    }
  }
}

PageRoute _getPageRoute({String routeName, Widget viewToShow}) {
  return MaterialPageRoute(
      settings: RouteSettings(
        name: routeName,
      ),
      builder: (_) => viewToShow);
}
