import 'package:authenticate/src/models/user.dart';
import 'package:flutter/cupertino.dart';

class AuthService extends ChangeNotifier {
  User _user;

  User get currentUser => _user;

  setUser(User user) {
    _user = user;
    notifyListeners();
  }
}
